<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        $circulos = [
            20,50,30
        ];
        
        $cuadrados = [
            20,30  
        ];
        ?>
        
        <?php
        foreach ($circulos as $value) {
            // inicio bucle para dibujar circulos
        ?>
            <?xml version="1.0" encoding="UTF-8" standalone="no"?>
            <svg width="140" height="140" viewBox="-10 -10 140 140" style="background-color: white">
            <circle cx="60" cy="60" r="<?= $value ?>" fill="black" />
            </svg>
        <?php
        // fin del bucle para circulos
        }
        
        
        foreach ($cuadrados as $valor) {
            // inicio bucle para dibujar cuadrados
        ?>        
            <?xml version="1.0" encoding="UTF-8" standalone="no"?>
            <svg width="140" height="140" viewBox="-10 -10 140 140" style="background-color: white">
            <rect x="10" y="10" width="<?= $valor ?>" height="<?= $valor ?>"/>
            </svg>
        <?php
        // fin del bucle cuadrados
        }
        ?>
        
    </body>
</html>
