<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <form>
            <label for="num1">Numero1</label>
            <input type="number" name="numeros[]" id="num1"/>
            <label form="num2">Numero 2</label>
            <input type="number" name="numeros[]" id="num2"/>            
            <button>Enviar</button>            
        </form>
        
        <?php
            if($_GET){
                // ahora leo los dos numeros como array
                $numeros = $_GET["numeros"];
                
                // creo un array asociativo para almacenar los resultados
                $resultados=[
                    "suma" => $numeros[0]+$numeros[1],
                    "resta" => $numeros[0]-$numeros[1],
                    "producto" => $numeros[0]*$numeros[1],
                    "cociente" => $numeros[0]/$numeros[1],
                    "potencia" => $numeros[0]**$numeros[1],
                    "modulo" => $numeros[0]%$numeros[1],
                ];
                        
                
        ?>              
        <table border="1" style="text-align: center; margin: 5px" cellspacing="3">
        <?php
            foreach ($resultados as $key => $value) {
                // principio del lazo para mostrar los resultados
        ?>
            <tr>
                <td><?= $key ?></td>
                <td><?= $value ?></td>
            </tr>
        <?php
            // final del lazo para mostrar los resultados
         }
        ?>
        </table>
        <?php
               
            }
        ?>
    </body>
</html>
