<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <form>
            <label for="num1">Numero1</label>
            <input type="number" name="numeros[]" id="num1"/>
            <label form="num2">Numero 2</label>
            <input type="number" name="numeros[]" id="num2"/>            
            <button>Enviar</button>            
        </form>
        
        <?php
            if($_GET){
                // ahora leo los dos numeros como array
                $numeros = $_GET["numeros"];           
                
        ?>
        <table border="1" style="text-align: center; margin: 5px" cellspacing="3">
                    <tr>
                        <td>Suma</td>
                        <td><?= $numeros[0]+$numeros[1] ?></td>
                    </tr>
                    <tr>
                        <td>Resta</td>
                        <td><?= $numeros[0]-$numeros[1] ?></td>
                    </tr>
                    <tr>
                        <td>Multiplicacion</td>
                        <td><?= $numeros[0]*$numeros[1] ?></td>
                    </tr>
                    <tr>
                        <td>Division</td>
                        <td><?= $numeros[0]/$numeros[1] ?></td>
                    </tr>
                    <tr>
                        <td>Potencia</td>
                        <td><?= $numeros[0]**$numeros[1] ?></td>
                    </tr>
                    <tr>
                        <td>Modulo</td>
                        <td><?= $numeros[0]%$numeros[1] ?></td>
                    </tr>
            </table>
        <?php
            }
        ?>
    </body>
</html>
